#!/bin/bash

if [ "$#" -lt 2 ]; then
    printf "Usage: \n\t ./deploy.sh i|u VERSION_NUMBER \n\t ./deploy.sh s"
    printf "\nOptions:"
    printf "\n\ti: Install and start network"
    printf "\n\tu: Upgrade and restart an installed and running network"
    exit
fi

ACTION=$1

PROJECT="auto-iot-hlf-proto"
VERSION=$2
ARCHIVE="$PROJECT@$VERSION.bna"

if [ "$ACTION" = "i" ]; then
    composer network install -a $ARCHIVE -c PeerAdmin@hlfv1 &&
        composer network start -n $PROJECT -c PeerAdmin@hlfv1 -V $VERSION -A admin -S adminpw &&
            composer card import -f admin@$PROJECT.card
elif [ "$ACTION" = "u" ]; then
    composer network install -a $ARCHIVE -c PeerAdmin@hlfv1
    composer network upgrade -c PeerAdmin@hlfv1 -n $PROJECT -V $VERSION
else
    echo "Invalid option given."
fi