'use strict';

/**
 * @param {edu.utd.auto.AddIncident} incidentData
 * @transaction
 */
async function addIncident(incidentData) {
    var incident;

    return getAssetRegistry('edu.utd.auto.Incident')
        .then(function(incidentRegistry){
            var  factory = getFactory();
            var  NS =  'edu.utd.auto';

            var  incidentId = incidentData.incidentId;
            
            incident = factory.newResource(NS,'Incident',incidentId);

            return getAssetRegistry('edu.utd.auto.Vehicle')
                .then(function(vehicleRegistry){
                    return vehicleRegistry.get(incidentData.insuredVehicle)
                    .then(function(incidentInsured) {
                        incident.insured = incidentInsured;
                        return vehicleRegistry.get(incidentData.involvedVehicle)
                            .then(function(incidentInvolved) {
                                incident.involved = incidentInvolved;
                                incident.severity = incidentData.severity;
                                incident.damage = incidentData.damage;
                                incident.time = incidentData.time;

                                var event = factory.newEvent(NS, 'IncidentOccurred');
                                event.incident = incident;
                                emit(event);

                                // Add incident to registry
                                return incidentRegistry.add(incident);
                            });
                    });
                });
            });
}
