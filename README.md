# Workbench

## Prerequisites: 
Have all the prerequisite tools and software installed: https://hyperledger.github.io/composer/latest/installing/installing-prereqs

Have all the developer tools installed as well: 
https://hyperledger.github.io/composer/latest/installing/development-tools.html

In case of errors – install Node Version Manager (nvm) and install node version v8.16.0 and re-run the necessary commands.

Another note make sure not to install as root (if you only have a root user, consider making other user accounts). Otherwise, do not run any command as root. Once the Hyperledger Fabric is installed and running, then our application is ready to be ran.

## Collecting Benchmark Results

### Setting up system to benchmark
```
cd dist
./build.sh
./deploy.sh i 0.0.11
./rest.sh
```

`deploy.sh` might return the error "Card already exists: admin@auto-iot-hlf-proto", but don't worry about it.

### Running benchmark

For each sampling rate A given, the benchmark system will spawn a certain number of users Z per second (Z is the arrival rate) until a certain number of concurrent users X is reached. Each user will alternate between a simple transaction (adding an asset to the registry) and a more complex one.

#### Change parameters of benchmark

Edit `workbench.csv.cfg` to change the parameters of the benchmark.
```
sampling_rates, A
concurrent_users, X
arrival_rate, Z
```
A is the sampling rate in milliseconds. X is the maximum number of concurrent users. Z is the number of concurrent users that arrive per second.

The following is an example `workbench.csv.cfg`.
```
sampling_rates, 125
concurrent_users, 500
arrival_rate, 10
```

For unclear reasons, machines with low RAM  (<=12 GB) or weak CPUs seem to have issues with running one Locust benchmark directly after another - Locust starts the benchmark and spawns the users but for some reason they don't execute any tasks. Those with bad laptops (including Haan's laptop in the lab) will unfortunately have to change the sampling rate after each test.

#### Start benchmark

```
cd locust
./workbench.py
```

Repeat, changing the sampling rate in `workbench.csv.cfg` each time.

## Viewing Benchmark Results

```
cd locust
./workbench.py
```

Repeat, changing the sampling rate in `workbench.csv.cfg` each time.

## Viewing Benchmark Results
```
cd flask
python3 server.py
```
Then go to your browser to localhost:5000/ and then you will see the page that lets you see the first screen that shows you a graph with a selected configuration. 
To see the second screen head to localhost:5000/recommendations and click the buttons with different configurations to get a recommendation. We tested many configurations and they all seem to be stable – however, it is not thoroughly tested for edge cases since different files have different outputs for that one-off chance of errors. 