from locust import HttpLocust, TaskSet, task
import time
import os
import random

import locust.stats
# Amount of time between each time it updates CSV files with statistics
locust.stats.CSV_STATS_INTERVAL_SEC = .25

'''
Defines tasks performed by each Locust AKA user.
User randomly selects from the list of tasks it has and executes it.
It then waits an amount of time and then repeat.
This repeats until the test ends
'''
class UserBehavior(TaskSet):
    asset = "/api/edu.utd.auto.Driver" # URL to make the HTTP request to
    transaction = "/api/edu.utd.auto.AddIncident"
        
    # Gets list of all assets (not a transaction)
    # VARIABLE: Can change probability distribution of a task being selected via @task(X) annotation
    # E.g. task marked task(2)  will be executed twice as much as task marked task(1)
    # @task(1)
    # def get_all_asset(self):
        # self.client.get(self.asset)

    # Posts an asset (transaction)
    @task(1)
    def post_asset(self):
        driverId = str(random.randint(0, 1000000000))
        name = "Driver_" + driverId
        self.client.post(self.asset, {"$class":"edu.utd.auto.Driver", "driverId":driverId, "name":name})

    @task(1)
    def post_custom_transaction(self):
        incidentId = str(random.randint(0, 1000000000))
        self.client.post(self.transaction, {"$class":"edu.utd.auto.AddIncident", "incidentId":incidentId, "insuredVehicle":"0", "involvedVehicle":"1", "severity":"NO_FIX", "damage":"eyeahehyah", "time":"2019-05-12T02:25:35.362Z"})

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    
    min_wait = 2000
    # min_wait = int(os.environ.get("LOCUST_SAMPLING_RATE"))
    max_wait = min_wait

    # REST server runs on localhost
    host = "http://localhost:3000"