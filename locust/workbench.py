#!/usr/bin/env python3

import subprocess
import time
import csv
import os
import requests

def set_up_preexisting_data():
    # Create two Drivers and two Vehicles to be involved in all the Incidents we generate
    driver_0 = {"$class":"edu.utd.auto.Driver", "driverId":"0", "name":"Stephen"}
    driver_1 = {"$class":"edu.utd.auto.Driver", "driverId":"1", "name":"Jeffrey"}

    vehicle_0 = {"$class":"edu.utd.auto.Vehicle", "vin":"0", "owner":"edu.utd.auto.Driver#0"}
    vehicle_1 = {"$class":"edu.utd.auto.Vehicle", "vin":"1", "owner":"edu.utd.auto.Driver#1"}

    requests.post("http://localhost:3000/api/edu.utd.auto.Driver", driver_0)
    time.sleep(.25)
    requests.post("http://localhost:3000/api/edu.utd.auto.Driver", driver_1)
    time.sleep(.25)
    requests.post("http://localhost:3000/api/edu.utd.auto.Vehicle", vehicle_0)
    time.sleep(.25)
    requests.post("http://localhost:3000/api/edu.utd.auto.Vehicle", vehicle_1)
    time.sleep(.25)


def main():
    set_up_preexisting_data()

    # Read configuration from file, namely sampling rate, hatch rate and maximum number of concurrent users
    config = open('workbench.csv.cfg', "r+")
    config_reader = csv.reader(config, delimiter=',')
    config_data = list(config_reader)
    
    sampling_rates = [int(i) for i in config_data[0][1:]]
    sampling_rates = sorted(sampling_rates, reverse=True) # Do longer sampling rates (e.g. 2000 ms between requests) before shorter (e.g. 125 ms)

    concurrent_users = int(config_data[1][1])
    
    arrival_rate = int(config_data[2][1])
    
    ramp_time = int(concurrent_users / arrival_rate)

    print("SAMPLING RATES " + str(sampling_rates))
    print("CONCURRENT USERS " + str(concurrent_users))
    print("ARRIVAL RATE " + str(arrival_rate))       

    print("################ STARTING BENCHMARK ################")

    for sampling_rate in sampling_rates:
        # Set sampling rate as environment variable
        os.environ["LOCUST_SAMPLING_RATE"] = str(sampling_rate) # This didn't work
        
        print("################ STARTING BENCHMARK FOR SAMPLING RATE " + str(sampling_rate) + " ################")

        # Create CSV file for sampling rate z named results_z.csv
        results_filename = "results_" + str(sampling_rate) + ".csv"

        # Open CSV file and write headers
        results_schema = "num_concurrent_users, num_requests, num_failures, median_latency_rt, average_latency_rt, minimum_latency_rt, maximum_latency_rt, avg_content_size, throughput_rps\n" 

        results = open(results_filename, 'w')
        results.write(results_schema)

        # Start the benchmark in locust
        locust_command = "locust -f ./locustfile.py --csv=benchmark --no-web -c " + str(concurrent_users) + " -r " + str(arrival_rate)
        locust = subprocess.Popen("exec " + locust_command, stdout = subprocess.PIPE, shell=True)

        time.sleep(2)

        # Open CSV file Locust is writing to named benchmark_requests.csv
        # Create if it doesn't exist
        benchmark_requests = open('benchmark_requests.csv', "r+")
        reader = csv.reader(benchmark_requests, delimiter=',')
        
        # Until benchmark terminates,
        for i in range(1, ramp_time + 1):
            
            # Copy performance metrics from Locust CSV to our CSV
            for row in reader:
                if row[1] == "Total":
                    results.write(str(i*arrival_rate) + ',')
                    # Metrics are in columns 2 - 9
                    for i in range(2, 9): 
                        results.write(str(row[i]) + ',')
                    results.write(str(row[9]) + '\n')

            benchmark_requests.seek(0)

            # Sleep for one second
            time.sleep(1)

        # Conclude the current test

        results.close()
        
        locust.terminate() # Allow Locust to shut off locusts and print results
        time.sleep(1) 

        locust.kill() # Kill Locust 
        # (Even after it's printed the results, it won't reliquish control of shell until it receives a kill signal)

        benchmark_requests.close()

        time.sleep(5)

        print("################ FINISHED BENCHMARK FOR SAMPLING RATE " + str(sampling_rate) + " ################")

    print("################ BENCHMARKING COMPLETE ################")

main()