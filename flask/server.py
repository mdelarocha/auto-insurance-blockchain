from flask import Flask, request, redirect, render_template
import pandas as pd
import pygal
from pygal.style import DarkSolarizedStyle
import os

# Start the flask app
app = Flask(__name__)

# Function to get all files under a directory
def get_all_dfs(hardware_config):
    dfs = list()
    # Get the path to the CSVS directory
    dir = os.path.dirname(__file__)
    directory = os.path.join(dir, 'CSVS', hardware_config)
    for root,dirs,files in os.walk(directory):
        for file in files:
            if file.endswith(".csv"):
                dfs.append(pd.read_csv(directory+"\\"+file, sep='\s*,\s*', header=0))
                
    return dfs

# Function to get all dataframes with data
def get_dfs(hardware_config,sample_rate):
     # Read all of the files as dataframes
    df = pd.read_csv('CSVS/' + hardware_config + '/results_' + sample_rate + '.csv', sep='\s*,\s*', header=0)
    
    return [df]

# Function to get the html of a given graph
def get_graph(hardware_config, sample_rate, metric, title):
     # Get the dataframs with the csv data
    dfs = get_dfs(hardware_config, sample_rate)
    if metric == 'throughput_rps':
        metricTitle = metric + " (rps)"
    else:
        metricTitle = metric + " (ms)"

    # Create the line chart
    line_chart = pygal.Line(width=1100, height=500,
                          explicit_size=True, title=title,
                          style=DarkSolarizedStyle,
                          disable_xml_declaration=True, 
                          y_title=metricTitle, 
                          x_title='Number of concurrent Users',
                          show_legend=False)

    line_chart.x_labels = map(str, range(10, 510, 10))

    line_chart.add(sample_rate + '_ms_rate', dfs[0][metric].tolist())

    return line_chart

# Function to get threshold for a given metric
def get_threshold(metric):
    threshold = 0
    if metric == "throughput_rps":
        threshold = 3.0
    elif metric == "median_latency_rt":
        threshold = 7000
    elif metric == "average_latency_rt":
        threshold = 5000
    elif metric == "minimum_latency_rt":
        threshold = 3000
    elif metric == "maximum_latency_rt":
        threshold = 9000
    
    return threshold

# Function to get a recommended sample rate given hardware, concurrent users, metric
def get_recommended_sample_rate(hardware_config, concurrent_users, metric):
    dfs = get_all_dfs(hardware_config)
    metric_averages_dictionary = dict()
    index = 125
    for df in dfs:
        row = df.loc[df['num_concurrent_users'] == int(concurrent_users)]
        metric_averages_dictionary[index] = row[metric].values
        index = index * 2
    
    if metric == "throughput_rps":
        highest = max(metric_averages_dictionary, key=metric_averages_dictionary.get)
        return str(highest) + " ms"
    else:
        lowest = min(metric_averages_dictionary, key=metric_averages_dictionary.get)
        return str(lowest) + " ms"

# Function to get a recommended number of concurrent users given hardware, sample rate, metric
def get_recommended_concurrent_users(hardware_config, sample_rate, metric):
    df = get_dfs(hardware_config, sample_rate)
    limit = get_threshold(metric)
    if metric == "throughput_rps":
        # max_val = df[0][metric].max()
        sub_series = df[0].loc[df[0][metric] >= limit]          
    else:
        # min_val = df[0][metric].min()
        sub_series = df[0].loc[df[0][metric] <= limit]

    row = sub_series.tail(1)
    return row['num_concurrent_users'].values[0]  

@app.route('/')
def index():
    return redirect("/benchmark-results")

@app.route('/benchmark-results', methods=['GET', 'POST'])
def get_results():
    hardware_config = request.form.get('hardware_config', "IntelCoreI5-2.6GHz-8GB")
    sample_rate = request.form.get('sample_rate', "1000")
    metric = request.form.get('metric', "throughput_rps")

    graph_title = metric.split('_')[0].capitalize() + " " + metric.split('_')[1].capitalize() + ' For ' + hardware_config + " at Sample Rate of " + sample_rate + "_ms"

    graph = get_graph(hardware_config, sample_rate, metric, graph_title)

    return render_template( 'benchmark_results.html', line_chart = graph, title = 'Benchmark Results', hc=hardware_config, sr=sample_rate, m=metric)

@app.route("/recommendations", methods=['GET', 'POST'])
def get_recommedations():
    if request.method == 'POST':
        if 'request-recommendation-sample' in request.form:
            hardware_config = request.form.get('hardware-config-sample', 'IntelCoreI5-2.6GHz-8GB')
            concurrent_users = request.form.get('concurrent-users-sample', '500')
            metric = request.form.get('metric-sample', "throughput_rps")
            r_sample_rate = get_recommended_sample_rate(hardware_config, concurrent_users, metric)
            title = "Recommended sample rate for " + hardware_config + " for metric " + metric + " with # of users " + concurrent_users
            return render_template('recommendations.html', sample_rate=r_sample_rate, concurrent_users=0, 
        s_title=title, c_title="Recommended # of Concurrent Users:")
        elif 'request-recommendation-concurrent' in request.form:
            hardware_config = request.form.get('hardware-config-concurrent', 'IntelCoreI5-2.6GHz-8GB')
            sample_rate = request.form.get('sample-rate-concurrent', '500')
            metric = request.form.get('metric-concurrent', "throughput_rps")
            r_concurrent_users = get_recommended_concurrent_users(hardware_config, sample_rate, metric)
            title = "Recommend # of concurrent users for " + hardware_config + " for metric " + metric + " with sample rate of " + sample_rate + " ms:"
            return render_template('recommendations.html', sample_rate=0, concurrent_users=r_concurrent_users, 
        s_title="Recommended sample rate:", c_title=title)
    else:
        return render_template('recommendations.html', sample_rate=0, concurrent_users=0, s_title="Recommended sample rate:",
    c_title="Recommended # of Concurrent Users:")
    

if __name__ == "__main__":
    app.run(debug=True)
